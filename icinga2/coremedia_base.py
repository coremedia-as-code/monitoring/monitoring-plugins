
import sys
import re
import traceback
import json
import socket
import math
from pyjolokia import Jolokia, JolokiaError
from errno import ECONNREFUSED
from enum import Enum

class CoreMediaBase(object):
    """

    """
    def __init__(self):
        self.verbose = True
        self.debug = False
        self.prefix = ''
        self.interval = 60.0
        self.jolokia_url = "http://localhost:8080/jolokia/"
        self.application = None
        self.jmx_port = None
        self.jmx_application = None

    def parse_url(self, url):
        """
          extract all data from an url
        """
        #print(mls_ior)

        pattern = (r'^'
                   r'((?P<schema>.+?)://)?'
                   r'((?P<user>.+?)(:(?P<password>.*?))?@)?'
                   r'(?P<host>.*?)'
                   r'(:(?P<port>\d+?))?'
                   r'(?P<path>/.*?)?'
                   r'(?P<query>[?].*?)?'
                   r'$')

        regex = re.compile(pattern)
        m = regex.match(url)
        d = m.groupdict() if m is not None else None

        return d


    def parse_bean_data(self, json_data, type, name = None, pool = None):
        """
          parse mbean data and return only one request
        """
        d = {}
        for v in json_data:
            status = v.get("status")
            if(status == 200):
                d = v.get('value')
                b = d.get('mbean')

                pattern = r".*type=%s$" % (type)

                if(name != None):
                    pattern = r".*name=%s,type=%s$" % (name, type)

                if(pool != None):
                    pattern = r".*pool=%s,type=%s$" % (pool, type)

                # print(b)
                # print(pattern)

                if(re.findall(pattern, d.get('mbean'))):
                    break
                else:
                    d = {}
            else:
                d = { "status": status }

        return d


    def jolokia_availability(self, jolokia_url = None, jmx_port = None ):
        """
        """

        if( jolokia_url == None):
            jolokia_url = self.jolokia_url

        if( jmx_port == None):
            jmx_port    = self.jmx_port

        jolokia_url_data = self.parse_url( jolokia_url)

        # print(jolokia_url_data)

        if( not self._portscan( jolokia_url_data.get('host'), int(jolokia_url_data.get('port')) ) ):
            self.logwarning( 'jolokia is currently not available' )
            return dict( { "status": 500, "message": 'jolokia is currently not available' } )

        return dict( { 'status': 200 } )


    def read_jmx_data(self, jolokia_data, jolokia_url = None, jmx_port = None ):
        """
          jolokia_data :

          data = {
              "Server": {
                "mbean": 'com.coremedia:type=Server,application=replication-live-server',
                "attribute": [
                  'RepositorySequenceNumber',
                  'RunLevelNumeric',
                  'RunLevel']
              },
              "Replicator": {
                "mbean": 'com.coremedia:type=Replicator,application=replication-live-server',
                "attribute": [
                  'ConnectionUp',
                  'ControllerState',
                  'LatestCompletedStampedNumber',
                  'MasterLiveServerIORUrl']
              }
          }
        """
        response = dict()

        if( jolokia_url == None):
            jolokia_url = self.jolokia_url

        if( jmx_port == None):
            jmx_port    = self.jmx_port

        jolokia_target = "service:jmx:rmi:///jndi/rmi://localhost:%d/jmxrmi" % (int(jmx_port))

        j4p = Jolokia( jolokia_url )
        # j4p.auth(httpusername='jolokia', httppassword='jolokia')
        j4p.config( ignoreErrors = 'true', ifModifiedSince = 'true', canonicalNaming = 'true' )
        j4p.target( url = jolokia_target )

        for instance in jolokia_data.keys():

            if isinstance(jolokia_data[instance], dict):
                mbean          = jolokia_data[instance]["mbean"]
                attribute_list = jolokia_data[instance]["attribute"]

                #print(mbean)
                #print(attribute_list)

                try:

                    j4p.add_request(
                        type = 'read',
                        mbean = mbean,
                        attribute = (",".join(attribute_list)) )
                except JolokiaError as error:
                    response = [{
                        "status": 500,
                        "message": str(error)
                    }]
                    # print(error)

        try:
            response = j4p.getRequests()
        except JolokiaError as error:
            response = [{
                "status": 500,
                "value": dict(),
                "request": dict(),
                "message": str(error)
            }]
            # print(error)

        # print( "{} {}".format(response, type(response)))
        if response:

            # rebuild dict
            for v in response:

                # print(json.dumps( v, indent = 2 ))

                status = v.get("status", 500)

                if( status == 200 ):
                    data = v.get('value')

                    data["timestamp"] = int(v.get("timestamp"))
                else:
                    v["value"] = {}
                    data = v.get('value')

                data["status"] = status
                if(v.get("request").get("mbean")):
                    data["mbean"] = v.get("request").get("mbean")

                try:
                    v.pop("request")
                    v.pop("stacktrace")
                    v.pop("error")
                except KeyError:
                    continue

        return response


    def _validate_application(self, valid_applications = [], application = None ):
        """

        """
        result  = False
        message = "'%s' is no valid application!" % (application)

        if( application == None or len(valid_applications) == 0 ):
            return result, "missing parameters"

        if application in valid_applications:
            result = True

            # cut instance number
            if re.match("^cae-live.*", application) is not None:
                application = "cae-live"

            # cut instance number
            if re.match("^replication-live-server.*", application) is not None:
                application = "replication-live-server"

            message = application

        return result, message


    def _detect_spring_boot(self):
        """

        """
        port = str(self.jmx_port)

        data = {
            # spring-boot
            "Connector": {
                "mbean": "Tomcat:type=Connector,port=%s" % ( port[:-2] + '80' ),
                "attribute": [
                  "port",
                  "stateName"]
            }
        }

        result      = self.read_jmx_data(data)
        server_data = self.parse_bean_data(result, type = "Connector")

        status = server_data.get('status')

        if( status == 200 ):
            return True

        return False


    def _portscan(self, target, port):
        try:
            # Create Socket
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            socketTimeout = 3
            s.settimeout(socketTimeout)
            s.connect((target,port))

            return True
            #print('port_scanner.is_port_opened() ' + str(port) + " is opened")
            #return port
        except socket.error as err:
            #print("{}".format(err))
            #if err.errno == ECONNREFUSED:
            #    print("ECONNREFUSED")

            return False
        except socket.error as msg:
            print( "Socket Error: %s" % msg )
        except TypeError as msg:
            print( "Type Error: %s" % msg )


    def _find_index(self, dicts, key, value):
        class Null: pass
        for i, d in enumerate(dicts):
            if d.get(key, Null) == value:
                return i
        else:
            raise ValueError('no dict with the key and value combination found')


    def _value_from_dic(self, dicts, key, value):
        """
        """
        i = self._find_index(dicts, key, value)
        return dicts[i]


    def _build_dict(self, seq, key):
        return dict((d[key], dict(d, index=index)) for (index, d) in enumerate(seq))


    def _convert_size(self, size_bytes):
       if size_bytes == 0:
           return "0B"
       size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
       i = int(math.floor(math.log(size_bytes, 1024)))
       p = math.pow(1024, i)
       s = round(size_bytes / p, 2)
       return "%s %s" % (s, size_name[i])


    def check_result(self, rc, message, metrics = None):
        """

        """
        prefix   = rc.name
        message  = '{} - {}'.format(prefix, message)

        print(message)
        sys.exit(rc.value)
