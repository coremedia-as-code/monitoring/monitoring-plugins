#!/usr/bin/env python3

"""
This plugin will return the runlevel state corresponding to the content server

"""
from __future__ import print_function

import sys
import argparse
import json
import coremedia_base

from enum import Enum

# -------------------------------------------------------------------------------------------------

class CheckState(Enum):
    OK = 0
    WARNING = 1
    CRITICAL = 2
    UNKNOWN = 3

# -------------------------------------------------------------------------------------------------

class Feeder(coremedia_base.CoreMediaBase):

    host        = None
    jmx_port    = None
    feeder_type = None

    PERFOMANCE_DATA = False

    CAE_FEEDER_LIVE_PORT    = 40899
    CAE_FEEDER_PREVIEW_PORT = 40799
    CONTENT_FEEDER_PORT     = 40499

    FEEDER_STATE_UNDEFINED    = -1
    FEEDER_STATE_STOPPED      = 0
    FEEDER_STATE_STARTING     = 1
    FEEDER_STATE_INITIALIZING = 2
    FEEDER_STATE_RUNNING      = 3
    FEEDER_STATE_FAILED       = 4
    FEEDER_STATE_UNKNOWN      = 99

    def __init__(self):

        coremedia_base.CoreMediaBase.__init__(self)

        self.args = {}
        self.parse_args()

        self.host          = self.args.host
        self.jmx_port      = self.args.jmxport
        self.feeder_type   = self.args.feeder
        self.jolokia_url   = "http://%s:8080/jolokia/" % (self.host)
        self.spring_boot   = self.args.spring_boot

        self.warning       = self.args.warning
        self.critical      = self.args.critical

        if(self.spring_boot == 'autodetect'):
            self.spring_boot     = self._detect_spring_boot()


    def parse_args(self):
        """
          parse commandline parameters
        """
        p = argparse.ArgumentParser(description = 'Icinga2 plugin to check CoreMedia CAE Feeder')

        p.add_argument('--host',
                            required = True,
                            help = 'hostname for the CAE Feeder')

        p.add_argument('--feeder',
                            required = True,
                            help = 'CAE Feeder type [live, preview]')

        p.add_argument('--jmxport',
                            help = 'JMX port [40899 for live, 40799 for preview]')

        p.add_argument('--warning',
                            type = int,
                            default = 1000,
                            help = 'warning how far the feeder may be behind the content server (default: 1000)')

        p.add_argument('--critical',
                            type = int,
                            default = 2500,
                            help = 'critical how far the feeder may be behind the content server (default: 2500)')
        p.add_argument('--spring_boot', required = False, help = 'autodetection of sprint boot', default = True )

        self.args = p.parse_args()


    def __define_jmx_port(self):
        """

        """
        state = CheckState.UNKNOWN
        message = "UNKNOWN: unknown feeder type"

        if(self.jmx_port == None):

            if(self.feeder_type == "live"):
                self.jmx_port = int(self.CAE_FEEDER_LIVE_PORT)
                return None, None
            elif(self.feeder_type == "preview"):
                self.jmx_port = int(self.CAE_FEEDER_PREVIEW_PORT)
                return None, None
            elif(self.feeder_type == "content-feeder" or self.feeder_type == "content"):
                self.jmx_port = int(self.CONTENT_FEEDER_PORT)
                return None, None

        return state, message


    def cae_feeder(self):
        """

        """
        self.__define_jmx_port()

        scan = self._portscan(self.host, self.jmx_port)

        if( scan == False ):
            self.check_result( CheckState.CRITICAL, "Feeder not running", {} )
            return # CheckState.CRITICAL, "Feeder not running"

        application = 'caefeeder-live'

        if(self.feeder_type == 'preview'):
            application = 'caefeeder-preview'

        data = {
            "Health": {
              "mbean": 'com.coremedia:type=Health,application=%s' % (application),
              "attribute": [
                'Healthy',
                'MaximumHeartBeat']
            },
            "ProactiveEngine": {
              "mbean": 'com.coremedia:type=ProactiveEngine,application=%s' % (application),
              "attribute": [
                'HeartBeat',
                'ValuesCount',
                'KeysCount',
                'Uptime',
                'QueueProcessedPerSecond',
                'SendSuccessTimeLatest']
            },
            "CapConnection": {
              "mbean": 'com.coremedia:type=CapConnection,application=%s' % (application),
              "attribute": [
                'Open',
                'CapListRepositoryHealthy',
                'ContentRepositoryAvailable',
                'ContentRepositoryHealthy',
                'Url']
            },
            "SolrIndexer": {
              "mbean": 'com.coremedia:type=SolrIndexer,application=%s' % (application),
              "attribute": [
                'Url',
                'Collection']
            }
        }

        if( self.spring_boot ):
            data["Health"]["mbean"]          = "com.coremedia:type=Health,application=coremedia"
            data["ProactiveEngine"]["mbean"] = "com.coremedia:type=ProactiveEngine,application=coremedia"
            data["CapConnection"]["mbean"]   = "com.coremedia:type=CapConnection,application=coremedia"
            data["SolrIndexer"]["mbean"]     = "com.coremedia:type=SolrIndexer,application=coremedia"

        result = self.read_jmx_data( jolokia_data = data, jmx_port = self.jmx_port )

        """
        data for the cae feeder (live / preview)
        """
        # {'Healthy': True, 'MaximumHeartBeat': 300000, 'timestamp': 1577345507, 'status': 200, 'mbean': 'com.coremedia:application=caefeeder-live,type=Health'}
        def get_health(json_data):
            """
            get health state
            """
            state         = CheckState.CRITICAL
            message       = "are not healthy!"

            health        = self.parse_bean_data(json_data, "Health")

            status        = health.get('status', 404)

            if(status == 200):

                state  = CheckState.OK
                message = "is Healthy" if health.get('Healthy') else "are not healthy!"

            return state, message


        # {'HeartBeat': 2590, 'Uptime': 71665152, 'QueueProcessedPerSecond': 0.111993305, 'SendSuccessTimeLatest': '2019-12-25T11:37:25Z', 'ValuesCount': 0, 'KeysCount': 0, 'timestamp': 1577345507, 'status': 200, 'mbean': 'com.coremedia:application=caefeeder-live,type=ProactiveEngine'}
        def get_feeder_elements(json_data):
            """
            read feeder elements
            """
            proactive_engine  = self.parse_bean_data(json_data, "ProactiveEngine")

            state             = CheckState.OK if proactive_engine['status'] == 200 else CheckState.CRITICAL

            if(state == CheckState.OK):
                _uptime          = proactive_engine['Uptime']
                # The heartbeat of this service: Milliseconds between now and the latest activity.
                # A low value indicates that the service is alive.
                # An constantly increasing value might be caused by a 'sick' or dead service
                heartbeat       = proactive_engine['HeartBeat']
                # Number of (valid) values. It is less or equal to 'keysCount'
                current_entries = proactive_engine['ValuesCount']
                # Number of (active) keys
                max_entries     = proactive_engine['KeysCount']
                # Latest time when any receiver has acknowledged an event successfully
                _latest_success  = proactive_engine['SendSuccessTimeLatest']
                # Number of processed queue items per second since startup
                _process_per_sec = proactive_engine['QueueProcessedPerSecond']

            if(max_entries == 0 and current_entries == 0):

                message = "%d Elements for Feeder available.\nThis feeder may be restarted or has just been installed." % (max_entries)

                return CheckState.UNKNOWN, message

            if(heartbeat >= 60000):

                message  = "Feeder Heartbeat is more then 1 minute old.\n"
                message += "Heartbeat %d ms" % (heartbeat)

                return CheckState.CRITICAL, message

            # calculate difference between key and values
            feeder_diffs    = max_entries - current_entries

            if(feeder_diffs == 0):

                message  = "all %d elements feeded.\n"
                message += "heartbeat %d ms"
                message += " | max=%d;current=%d;diff=%d;heartbeat=%d"

                message = message % (max_entries, heartbeat, max_entries, current_entries, feeder_diffs, heartbeat)
            else:

                message  = "%d elements of %d feeded. "
                message += "%d elements left.\n"
                message += "heartbeat %d ms"
                message += " | max=%d;current=%d;diff=%d;heartbeat=%d"

                message = message % (current_entries, max_entries, feeder_diffs, heartbeat, max_entries, current_entries, feeder_diffs, heartbeat)

            if(feeder_diffs >= self.critical):
                state = CheckState.CRITICAL
            elif(feeder_diffs >= self.warning):
                state = CheckState.WARNING
            else:
                state = CheckState.OK

            return state, message

        state, healthy      = get_health(result)
        message_health      = "Feeder %s" % (healthy)

        if(state == CheckState.OK):

            state, message_feeder = get_feeder_elements(result)

            message_health +=  ", " + message_feeder

        self.check_result( state, message_health, {} )


    def content_feeder(self):
        """

        """
        state   = CheckState.CRITICAL
        message = "Feeder are in <b>unknown</b> state."

        self.__define_jmx_port()

        scan = self._portscan(self.host, self.jmx_port)

        if( scan == False ):
            return CheckState.CRITICAL, "Feeder not running"

        application = 'content-feeder'

        data = {
            "Feeder": {
              # State:
              #   Return the state of the feeder
              # StateNumeric:
              #   Return the state of the feeder with 0=stopped, 1=starting, 2=initializing, 3=running, 4=failed
              # PendingEvents
              #   Number of events behind most recent event
              # PersistedEvents
              #   Returns the persisted events in the last interval.
              # IndexBatches
              #   Returns the number of persisted batches in the last interval.
              # IndexOpenBatches
              #   Return the number of open batches, i.e. the number of sent batches for which we haven't received a persisted callback yet.
              # IndexDocuments
              #   Returns the number of persisted index documents in the last interval.
              #
              "mbean": 'com.coremedia:type=Feeder,application=%s' % (application),
              "attribute": [
                'State',
                'StateNumeric',
                'PendingEvents',
                'PersistedEvents',
                'IndexBatches',
                'IndexOpenBatches',
                'IndexDocuments']
            },
            "CapConnection": {
              "mbean": 'com.coremedia:type=CapConnection,application=%s' % (application),
              "attribute": [
                'Open',
                'CapListRepositoryHealthy',
                'ContentRepositoryAvailable',
                'ContentRepositoryHealthy',
                'Url']
            },
            "SolrIndexer": {
              "mbean": 'com.coremedia:type=SolrIndexer,application=%s' % (application),
              "attribute": [
                'Url',
                'Collection']
            }
        }

        if( self.spring_boot ):
            data["Feeder"]["mbean"]          = "com.coremedia:type=Feeder,application=coremedia"
            data["CapConnection"]["mbean"]   = "com.coremedia:type=CapConnection,application=coremedia"
            data["SolrIndexer"]["mbean"]     = "com.coremedia:type=SolrIndexer,application=coremedia"

        result = self.read_jmx_data( jolokia_data = data, jmx_port = self.jmx_port )

        feeder = self.parse_bean_data(result, "Feeder")

        status = feeder.get('status', 404)

        if( status == 200):
            state  = CheckState.OK

        # make pretty json
        # print(json.dumps( feeder, indent = 2 ))

        feeder_state_numeric = self.FEEDER_STATE_UNDEFINED

        if(state == CheckState.OK):
            feeder_state         = feeder.get('State', None)
            feeder_state_numeric = feeder.get('StateNumeric', -1)

            if(feeder_state_numeric == self.FEEDER_STATE_UNDEFINED):

                if(feeder_state == "initializing"):
                    message = "Feeder are in <b>initializing</b> state."
                    state  = CheckState.WARNING
                elif(feeder_state == "running"):
                    message = "Feeder are <b>running</b>."
                    state  = CheckState.OK
                else:
                    message = "Feeder are in <b>unknown</b> state."
                    state  = CheckState.CRITICAL

            else:
                if(feeder_state_numeric == self.FEEDER_STATE_STOPPED):
                    message = "Feeder are in <b>stopped</b> state."
                    state  = CheckState.WARNING
                elif(feeder_state_numeric == self.FEEDER_STATE_STARTING):
                    message = "Feeder are in <b>starting</b> state."
                    state  = CheckState.UNKNOWN
                elif(feeder_state_numeric == self.FEEDER_STATE_INITIALIZING):
                    message = "Feeder are in <b>initializing</b> state."
                    state  = CheckState.UNKNOWN
                elif(feeder_state_numeric == int(self.FEEDER_STATE_RUNNING)):
                    message = "Feeder are <b>running</b>."
                    state  = CheckState.OK
                else:
                    message = "Feeder are in <b>failed</b> state."
                    state  = CheckState.CRITICAL

        if(feeder_state_numeric == int(self.FEEDER_STATE_RUNNING) or feeder_state_numeric == self.FEEDER_STATE_INITIALIZING ):
            """
            """
            pending_events       = feeder.get('PendingEvents')
            persistent_index_doc = feeder.get('IndexDocuments')
            persisted_events     = feeder.get('PersistedEvents')
            persisted_batches    = feeder.get('IndexBatches')
            open_batches         = feeder.get('IndexOpenBatches')

            message += "\nPending Events: %d, Open Batches: %d, persisted batches: %d,  persisted events: %d, persisted index documents: %d"
            message = message % ( pending_events, open_batches, persisted_batches, persisted_events, persistent_index_doc )

            if(self.PERFOMANCE_DATA is True):
                message += " | pending_events=%d"
                message = message % ( pending_events, open_batches, persisted_batches, persisted_events, persistent_index_doc )

        self.check_result( state, message, {} )


    def check(self):
        if(self.feeder_type in ('live', 'preview')):
            self.cae_feeder()
        else:
            self.content_feeder()


# -------------------------------------------------------------------------------------------------

c = Feeder()
c.check()

# -------------------------------------------------------------------------------------------------
