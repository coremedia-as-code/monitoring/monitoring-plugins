#!/usr/bin/env python3

from __future__ import print_function

import sys
import argparse
import json
import coremedia_base

from enum import Enum

# -------------------------------------------------------------------------------------------------

class CheckState(Enum):
    OK = 0
    WARNING = 1
    CRITICAL = 2
    UNKNOWN = 3

# -------------------------------------------------------------------------------------------------

class Runlevel(coremedia_base.CoreMediaBase):

    host        = None
    jmx_port    = 0
    application = None

    def __init__(self):

        self.args = {}
        self.parse_args()

        self.host          = self.args.host
        self.jmx_port      = self.args.jmxport
        self.application   = self.args.application
        self.jolokia_url   = "http://%s:8080/jolokia/" % (self.host)
        self.spring_boot   = self.args.spring_boot

        if(self.spring_boot == 'autodetect'):
            self.spring_boot     = self._detect_spring_boot()


    def parse_args(self):
        """
          parse commandline parameters
        """
        p = argparse.ArgumentParser(description = 'Icinga2 plugin to check CoreMedia Runlevel')

        p.add_argument('--host',
                            required = True,
                            help = 'hostname' )

        p.add_argument('--jmxport',
                            required = True,
                            type = int,
                            help = 'JMX port (hint: CMS = 40199, MLS = 40299, RLS = 42099)' )

        p.add_argument('--application',
                            required = True,
                            help = 'named application' )

        p.add_argument('--spring_boot', required = False, help = 'autodetection of sprint boot', default = True )

        self.args = p.parse_args()




    def __validate_application(self):
        """

        """
        valid_applications = [
            "content-management-server",
            "master-live-server",
            "replication-live-server"]

        return self._validate_application( valid_applications, self.application )


    def __get_data(self):
        """
        read data from the jolokia server
        """
        valid, application = self.__validate_application()

        if(valid is False):
            print(application)
            sys.exit(CheckState.CRITICAL)

        data = {
            "Server": {
              "mbean": 'com.coremedia:type=Server,application=%s' % (application),
              # RunLevel
              #   the current run level of the Content Server, which is one of 'offline', 'maintenance', 'administration', or 'online'
              # RunLevelNumeric
              #   the current run level of the Content Server, which is one of 0='offline', 1='maintenance', 2='administration', or 3='online'
              # Uptime
              #   the time since the Content Server was started in milliseconds
              #
              "attribute": [
                'RunLevelNumeric',
                'RunLevel',
                'Uptime'
              ]
            }
        }

        if( self.spring_boot ):
            data["Server"]["mbean"] = "com.coremedia:type=Server,application=coremedia"

        return self.read_jmx_data(data)

# -------------------------------------------------------------------------------------------------

    def get_runlevel(self):
        """
          get cap connection status
        """
        state = CheckState.UNKNOWN
        message = "no Runlevel found."

        data          = self.__get_data()
        runlevel_data = self.parse_bean_data(data, "Server")

        runlevel_status        = runlevel_data.get('status')

        if(runlevel_status == 200):
            runlevel         = runlevel_data.get("RunLevel")
            runlevel_numeric = runlevel_data.get("RunLevelNumeric")


            if(runlevel_numeric == -1):

                if(runlevel == "offline"):
                    state  = CheckState.CRITICAL
                elif(runlevel == "online"):
                    state  = CheckState.OK
                elif(runlevel == "administration"):
                    state  = CheckState.WARNING
                else:
                    state  = CheckState.CRITICAL
            else:

                if(runlevel_numeric == 0):
                    runlevel = "stopped"
                    state  = CheckState.WARNING
                elif(runlevel_numeric == 1):
                    runlevel = "starting"
                    state  = CheckState.UNKNOWN
                elif(runlevel_numeric == 2):
                    runlevel = "initializing"
                    state  = CheckState.UNKNOWN
                elif(runlevel_numeric == 3):
                    runlevel = "running"
                    state  = CheckState.OK
                else:
                    runlevel = "failed"
                    state  = CheckState.CRITICAL

            message = "Runlevel in <b>%s</b> mode." % (runlevel)

        self.check_result( state, message, {} )


    def check(self):
        self.get_runlevel()


# -------------------------------------------------------------------------------------------------

c = Runlevel()
c.check()

# -------------------------------------------------------------------------------------------------
