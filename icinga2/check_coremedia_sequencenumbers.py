#!/usr/bin/env python3

from __future__ import print_function

import sys
import argparse
import json
import coremedia_base

from datetime import date, datetime, timedelta
import time
from enum import Enum

# -------------------------------------------------------------------------------------------------

class CheckState(Enum):
    OK = 0
    WARNING = 1
    CRITICAL = 2
    UNKNOWN = 3

# -------------------------------------------------------------------------------------------------

class Syncronity(coremedia_base.CoreMediaBase):

    host        = None
    jmx_port    = 0
    application = None

    MLS_PORT = 40299
    RLS_PORT = 42099

    RUNLEVEL_OFFLINE = 0
    RUNLEVEL_MAINTENANCE = 1
    RUNLEVEL_ADMINISTRATION = 2
    RUNLEVEL_ONLINE = 3

    def __init__(self):
        """
        """
        coremedia_base.CoreMediaBase.__init__(self)

        self.args = {}
        self.parse_args()

        self.host        = self.args.host
        self.sequence_warning   = self.args.sequence_warning
        self.sequence_critical  = self.args.sequence_critical
        self.events_warning     = self.args.events_warning
        self.events_critical    = self.args.events_critical
        self.jmx_port           = self.RLS_PORT
        self.application        = None
        self.jolokia_url        = "http://%s:8080/jolokia/" % (self.host)

        self.spring_boot   = self.args.spring_boot

        if(self.spring_boot == 'autodetect'):
            self.spring_boot     = self._detect_spring_boot()


    def parse_args(self):
        """
          parse commandline parameters
        """
        p = argparse.ArgumentParser(description = 'Icinga2 plugin to check CoreMedia RLS sequence numbers')

        p.add_argument('--host',
                            required = True,
                            help = 'hostname for the RLS Server' )

        p.add_argument('--warning',
                            type = int,
                            dest = 'sequence_warning',
                            default = 1000,
                            help = 'warning, how far the RLS may lag behind the MLS (default: 1000)' )

        p.add_argument('--critical',
                            type = int,
                            dest = 'sequence_critical',
                            default = 2500,
                            help = 'critical, how far the RLS may lag behind the MLS (default: 2500)' )

        p.add_argument('--ewarning',
                            type = int,
                            dest = 'events_warning',
                            default = 14,
                            help = 'warning, how many hours the RLS may lag behind the MLS with events (default: 14)' )

        p.add_argument('--ecritical',
                            type = int,
                            dest = 'events_critical',
                            default = 24,
                            help = 'critical, how many hours the RLS may lag behind the MLS with events (default: 24)' )

        p.add_argument('--spring_boot', required = False, help = 'autodetection of sprint boot', default = True )

        self.args = p.parse_args()

    # https://stackoverflow.com/a/42311493
    #
    # -> STRING   Thu Jan 01 00:00:00 GMT 1970
    # <- DATETIME 1970-01-01 01:00:00
    def __convert_to_datetime(self, datetime_string):
        """
        """
        # split on spaces
        ts = datetime_string.split()
        # remove the timezone
        tz = ts.pop(4)
        # parse the timezone to minutes and seconds
        tz_offset = -1 *60 # int(tz[-6] + str(int(tz[-5:-3]) * 60 + int(tz[-2:])))
        # return a datetime that is offset
        return datetime.strptime(' '.join(ts), '%a %b %d %H:%M:%S %Y') - timedelta(minutes=tz_offset)

    # -> FLOAT    1582096445.761112
    # <- TUPLE   ('50 years 2 months 1 day 7 hours 16 minutes', [50, 2, 1, 7, 16])
    def __td_format( self, seconds ):
        """
        """
        periods = [
            ('year',        60*60*24*365),
            ('month',       60*60*24*30),
            ('day',         60*60*24),
            ('hour',        60*60),
            ('minute',      60)
        ]

        plain = { "year": 0, "month": 0, "day": 0, "hour": 0 }


        strings=[]
        for period_name, period_seconds in periods:

            if seconds > period_seconds:
                period_value , seconds = divmod(seconds, period_seconds)
                has_s = 's' if period_value > 1 else ''
                strings.append( "%s %s%s" % ( int(period_value), period_name, has_s ) )
                plain[ period_name ] = int(period_value)

        return " ".join( strings ) , plain

    # -> STRING   Thu Jan 01 00:00:00 GMT 1970
    # <- TUPLE    (1582096707.142191, datetime.datetime(1970, 1, 1, 1, 0))
    def __event_time( self, event_time ):
        """
        """
        current_datetime = datetime.now()
        event_time       = self.__convert_to_datetime( event_time )
        # timestamp        = event_time.timestamp()

        difference       = current_datetime - event_time
        duration_in_s    = difference.total_seconds()

        # seconds          = abs(int(duration_in_s))
        #
        # years, _         = divmod(seconds, 31556952)
        # days, seconds    = divmod(seconds, 86400)
        # hours, seconds   = divmod(seconds, 3600)
        # minutes, seconds = divmod(seconds, 60)
        # hours, minutes   = divmod(minutes, 60)
        # days, hours      = divmod(hours, 24)
        # weeks, days      = divmod(days, 7)
        #
        # hours_ago   = int( duration_in_s // 3600)
        # minutes_ago = int( (duration_in_s % 3600) // 60 )
        # seconds_ago = int( (duration_in_s % 3600) % 60 )

        return duration_in_s, event_time



    # {'LatestCompletedStampedNumber': 5, 'ConnectionUp': True, 'ControllerState': 'running', 'LatestIncomingStampedNumber': 5, 'MasterLiveServerIORUrl': 'http://mls.cm.local:40280/master-live-server/ior', 'Enabled': True, 'PipelineUp': True, 'timestamp': 1577361818, 'status': 200}
    def __rls_data(self):
        """

        """
        scan = self._portscan(self.host, self.jmx_port)

        if( scan == False ):
            x = '[ { "status": 500 } ]'
            return json.loads( x )

        data = {
            "Server": {
              "mbean": 'com.coremedia:type=Server,application=replication-live-server',
              "attribute": [
                'RepositorySequenceNumber',
                'RunLevelNumeric',
                'RunLevel']
            },
            "Replicator": {
              "mbean": 'com.coremedia:type=Replicator,application=replication-live-server',
              # CompletedStartTime
              #   Date of the first completion of an event since server startup
              # IncomingStartTime
              #   Date of the first arrival of an incoming event since server startup
              # LatestCompletedArrival
              #   Date of the latest completion of an event
              # LatestIncomingArrival
              #   Date of the latest arrival of an incoming event
              # LatestCompletionDuration
              #   Difference in milliseconds between the arrival and the completion of the latest completed event since server startup
              #
              "attribute": [
                'ConnectionUp',
                'ControllerState',
                'CompletedStartTime',
                'IncomingStartTime',
                'LatestCompletedArrival',
                'LatestIncomingArrival',
                'LatestCompletionDuration',
                'LatestCompletedStampedNumber',
                'MasterLiveServerIORUrl']
            }
        }

        if( self.spring_boot ):
            data["Server"]["mbean"]     = "com.coremedia:type=Server,application=coremedia"
            data["Replicator"]["mbean"] = "com.coremedia:type=Replicator,application=coremedia"

        return self.read_jmx_data( jolokia_data = data, jmx_port = self.RLS_PORT )


    # {'RunLevel': 'online', 'RunLevelNumeric': 3, 'RepositorySequenceNumber': 5, 'timestamp': 1577361818, 'status': 200}
    def __mls_data(self, mls_host):
        """

        """
        scan = self._portscan(mls_host, 8080)

        if( scan == False ):
            x = '[ { "status": 500 } ]'
            return json.loads( x )

        data = {
            "Server": {
              "mbean": 'com.coremedia:type=Server,application=master-live-server',
              "attribute": [
                'RepositorySequenceNumber',
                'RunLevelNumeric',
                'RunLevel']
            }
        }

        if( self.spring_boot ):
            data["Server"]["mbean"]     = "com.coremedia:type=Server,application=coremedia"

        return self.read_jmx_data( jolokia_data = data, jolokia_url = "http://%s:8080/jolokia/" % (mls_host) , jmx_port = self.MLS_PORT )

# -------------------------------------------------------------------------------------------------

    def get_sequencenumbers(self):
        """

        """
        state             = CheckState.UNKNOWN
        message           = "no data found."

        rls_data          = self.__rls_data()

        rls_server        = self.parse_bean_data(rls_data, "Server")
        rls_replicator    = self.parse_bean_data(rls_data, "Replicator")


        def __event_state( years, month, day, hours ):
            """

            """
            message = ""
            state = CheckState.OK

            if( years > 0 or month > 0 or day > 0 ):
                message = " (CRITICAL)"
                state = CheckState.CRITICAL
            else:
                if(int(hours) >= self.events_critical):
                    message = " (CRITICAL)"
                    state = CheckState.CRITICAL
                elif( int(hours) >= self.events_warning):
                    message = " (WARNING)"
                    state = CheckState.WARNING
                else:
                    messahe = ""
                    state = CheckState.OK

            return state, message


        # make pretty json
        # print(type( rls_server))
        # print(json.dumps( rls_server, indent = 2 ))
        # print(json.dumps( rls_replicator, indent = 2 ))

        if(len(rls_server) == 0 or len(rls_replicator) == 0):
            message = "replication-live-server are not available or not in runlevel online"
            state = CheckState.CRITICAL
        else:

          rls_server_status = rls_server.get('status', 404)

          if( rls_server_status == 200 ):

              rls_runlevel          = rls_server.get('RunLevelNumeric')
              rls_replicator_status = rls_replicator.get('status', 404)

              if( rls_replicator_status == 200 and rls_runlevel == self.RUNLEVEL_ONLINE ):
                  """

                  """
                  mls_ior                    = rls_replicator.get('MasterLiveServerIORUrl')
                  connected_to_mls           = rls_replicator.get('ConnectionUp')
                  controler_state            = rls_replicator.get('ControllerState')
                  latest_stamped_number      = rls_replicator.get('LatestCompletedStampedNumber')

                  completed_start_time       = rls_replicator.get('CompletedStartTime')
                  incoming_start_time        = rls_replicator.get('IncomingStartTime')
                  latest_completed_arrival   = rls_replicator.get('LatestCompletedArrival')
                  latest_incoming_arrival    = rls_replicator.get('LatestIncomingArrival')
                  latest_completion_duration = rls_replicator.get('LatestCompletionDuration')

                  duration_in_s, event_time  = self.__event_time( latest_completed_arrival )

                  mls_url_data               = self.parse_url(mls_ior)
                  mls_host                   = mls_url_data.get('host')
                  mls_data                   = self.__mls_data(mls_host)
                  mls_server                 = self.parse_bean_data(mls_data, "Server")

                  mls_status                 = mls_server.get("status", 404)
                  mls_runlevel               = mls_server.get('RunLevelNumeric')
                  repository_seq_number      = mls_server.get('RepositorySequenceNumber')

                  if( mls_status == 200 and mls_runlevel == self.RUNLEVEL_ONLINE ):

                      diff = repository_seq_number - latest_stamped_number

                      formated_date, plain_date_data = self.__td_format( duration_in_s )

                      year              = plain_date_data.get('year')
                      month             = plain_date_data.get('month')
                      day               = plain_date_data.get('day')
                      hour              = plain_date_data.get('hour')

                      last_completed_event_message   = "the last completed event was %s ago (%s)"
                      last_completed_event_message   = last_completed_event_message % ( formated_date, event_time.strftime('%d.%m.%Y %H:%M:%S') )

                      state, msg = __event_state(year, month, day, hour)

                      last_completed_event_message += msg

                      if( diff == 0 ):
                          message  = "sequence numbers between MLS and RLS are equal\n"
                          message += last_completed_event_message

                      else:
                          message  = "sequence numbers between MLS and RLS differ %d\n" % ( diff )
                          message += last_completed_event_message

                          if( diff > self.sequence_critical ):
                              state = CheckState.CRITICAL
                          elif( diff > self.sequence_warning or diff == self.sequence_warning ):
                              state = CheckState.WARNING
                          else:
                              state = CheckState.OK

                  else:
                      message = "replication-live-server is available, <b>but the master-live-server '{}' is not available or not in runlevel online</b>".format(mls_host)
                      state = CheckState.WARNING

              else:
                  message = "replication-live-server are <b>not available or not in runlevel online</b>"
                  state = CheckState.CRITICAL

        self.check_result( state, message, {} )


    def check(self):
        self.license()


# -------------------------------------------------------------------------------------------------

c = Syncronity()
c.get_sequencenumbers()

# -------------------------------------------------------------------------------------------------
