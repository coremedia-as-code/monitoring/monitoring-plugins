#!/usr/bin/env python3

from __future__ import print_function

import sys
import re
import logging
import argparse
import json
import coremedia_base

from requests import get
from requests.exceptions import ConnectionError
from enum import Enum

# -------------------------------------------------------------------------------------------------

class CheckState(Enum):
    OK = 0
    WARNING = 1
    CRITICAL = 2
    UNKNOWN = 3

# -------------------------------------------------------------------------------------------------

class Solr(coremedia_base.CoreMediaBase):

    solr_port         = 40080
    solr_url          = "http://delivery.cm.local:%d/solr" % ( solr_port )
    solr_core_path    = "admin/cores?action=STATUS&wt=json"
    solr_metrics_path = "admin/metrics?wt=json&type=counter,gauge&group=core&prefix=CORE,REPLICATION"
    solr_replication_detail = "%s/replication?command=details"


    def __init__(self):
        """

        """
        coremedia_base.CoreMediaBase.__init__(self)

        self.args = {}
        self.parse_args()

        self.solr_host = self.args.solr_host
        self.solr_port = self.args.solr_port
        self.solr_core = self.args.solr_core

        self.solr_url  = "http://%s:%d/solr" % ( self.solr_host, self.solr_port )


        self.warning       = self.args.warning
        self.critical      = self.args.critical

        self.spring_boot   = self.args.spring_boot

        if(self.spring_boot == 'autodetect'):
            self.spring_boot     = self._detect_spring_boot()


    def parse_args(self):
        """
          parse commandline parameters
        """
        p = argparse.ArgumentParser(description = 'Icinga2 plugin to check CoreMedia Solr cores')


        p.add_argument('--host',
                            dest = 'solr_host',
                            default = 'localhost',
                            help = 'Solr Host (default: localhost)' )

        p.add_argument('--port',
                            dest = 'solr_port',
                            type = int,
                            default = 40080,
                            help = 'Solr Port (default: 40080)' )

        p.add_argument('-C', '--core',
                            dest = 'solr_core',
                            action='store',
                            nargs = '*',
                            type = str,
                            help = 'Solr Core (Example: --core live preview studio (default: live))' )

        p.add_argument('--warning',
                            type = int,
                            default = 2500,
                            help = 'warning how far the solr index may lag behind the master (default: 2500)')

        p.add_argument('--critical',
                            type = int,
                            default = 4000,
                            help = 'critical how far the solr index may lag behind the master (default: 4000)')


        p.add_argument('--spring_boot', required = False, help = 'autodetection of sprint boot', default = True )

        self.args = p.parse_args()


    def __request_data(self, path):
      """
        executes get request to retrieve data from given url
      """

      logging.captureWarnings(True)

      # print( "%s/%s" % ( self.solr_url, path ) )

      try:
          response = get( "%s/%s" % ( self.solr_url, path ) )
          # check response content type to determine which actuator api to be used
          if response.ok:
              return CheckState.OK, response, None
          else:
              return None, Exception(response.status_code, self.solr_url), None

      except ConnectionRefusedError as e:
          logging.error('Failed to establish a new connection: {} '.format(self.solr_url) )
          return CheckState.CRITICAL, None, msg

      except ConnectionError as e:
          msg = 'error fetching data from {}' . format(self.solr_url)
          #logging.error('error fetching data from {}'.format(self.solr_url))
          #logging.debug('error fetching data from {}'.format(self.solr_url))
          return CheckState.CRITICAL, None, msg

      finally:
          logging.captureWarnings(False)

# -------------------------------------------------------------------------------------------------

    def __solr_cores(self):
      """
      """
      state, response, error_message = self.__request_data(self.solr_core_path)

      result = []
      # message = ''

      if( state == CheckState.OK ):

          content_type = response.headers['Content-Type']
          content_body = response.text

          data = json.loads(content_body)
          core_state = data.get("status")

          for solr_core in core_state.keys():
              d            = core_state.get(solr_core)

              core_uptime  = d.get("uptime")
              core_name    = d.get('name')
              core_size    = d.get('index').get('sizeInBytes')

              result.append({
                "name": core_name,
                "uptime": core_uptime,
                "size": core_size
              })

      return state, result


    def __solr_metrics(self):
      """
      """
      state, response, error_message = self.__request_data(self.solr_metrics_path)

      if( state == CheckState.OK ):

          content_type = response.headers['Content-Type']
          content_body = response.text

          data         = json.loads(content_body)

          # print(json.dumps( data, indent = 2 ))


    def __solr_replication_status(self, core):
      """
      """
      state, response, error_message = self.__request_data(self.solr_replication_detail % (core) )
      result = []
      message = ''

      if( state == CheckState.OK ):
          """
          """
          content_type = response.headers['Content-Type']
          content_body = response.text

          data    = json.loads(content_body)
          details = data.get('details')

          is_master = True if details.get("isMaster") == "true" else False
          is_slave  = True if details.get("isSlave") == "true" else False
          slave_data =  details.get('slave', {})

          # remove commits
          details.pop('commits')

          solr_index_version   = details.get('indexVersion')
          master_index_version = 0

          replication_error    = None

          if( is_slave and slave_data ):
              """
                her comes an slave ...
              """
              master_url           = details.get('slave').get('masterUrl')
              master_url_data      = self.parse_url(master_url)


              if( details.get("slave").get("ERROR") ):
                  replication_error = details.get("slave").get("ERROR")

                  state = CheckState.CRITICAL

                  result = {
                      "master": is_master,
                      "slave": is_slave,
                      "version": solr_index_version,
                      "master_url": master_url_data.get('host'),
                      "master_version": master_index_version,
                      "error_msg": replication_error
                  }

                  return state, result

              if( details.get("slave").get("masterDetails") ):
                details.get("slave").get("masterDetails").pop('commits')
                master_index_version = details.get('slave').get('masterDetails').get('indexVersion')

              result = {
                core: {
                  "master": is_master,
                  "slave": is_slave,
                  "version": solr_index_version,
                  "master_url": master_url_data.get('host'),
                  "master_version": master_index_version,
                  "error_msg": replication_error
                }
              }

          else:
              result = {
                core: {
                    "master": is_master,
                    "slave": is_slave,
                    "version": solr_index_version,
                }
              }


      return state, result


    def status(self):
      """

      """
      state   = CheckState.CRITICAL
      message = 'Solr on %s are not available' % ( self.solr_host )

      state, cores = self.__solr_cores()

      #print(self.solr_core)
      #print(len(self.solr_core))
      if( self.solr_core != None and len(self.solr_core) == 1 ):
          self.solr_core = self.solr_core[0]

          r = self._value_from_dic( cores, 'name', self.solr_core)
          cores = [ r ]

      if( state == CheckState.OK ):
          message = 'Solr are available'
          message_2 = []
          for core in cores:

              core_name   = core.get('name')
              core_uptime = core.get('uptime')
              core_size   = core.get('size')

              state, result = self.__solr_replication_status( core_name )

              if( state == CheckState.OK ):

                  core_data      = result.get(core_name)

                  is_slave       = core_data.get("slave")
                  is_master      = core_data.get("master")
                  version        = core_data.get("version")
                  master_version = core_data.get("master_version")
                  master_url     = core_data.get("master_url")

                  message_2.append('<b>%s</b> for core %s (size: %s)' % ( 'master' if is_master else 'slave', core_name, self._convert_size(core_size) ) )

                  if(is_slave):
                      version_diff   = (int(master_version) - int(version))

                      if(version_diff >= self.critical):
                          state = CheckState.CRITICAL
                      elif(version_diff >= self.warning):
                          state = CheckState.WARNING
                      else:
                          state = CheckState.OK

                      if( (int(master_version) - int(version) ) == 0 ):
                          message += " and <b>in sync</b> with his master %s" % (master_url)
                      else:
                          message += " and <b>not in sync</b> with his master %s (version differs %d)" % (master_url, version_diff)

              else:
                  message_2 = ''

          message = message + "\n" + "<br>".join(message_2)

      self.check_result( state, message, {} )


    def check(self):
        self.status()


# -------------------------------------------------------------------------------------------------

c = Solr()
c.check()

# -------------------------------------------------------------------------------------------------
