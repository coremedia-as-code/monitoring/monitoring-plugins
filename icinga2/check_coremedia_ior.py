#!/usr/bin/env python3

from __future__ import print_function

import sys
import re
import logging
import argparse

from requests import get
from requests.exceptions import ConnectionError

from enum import Enum

# -------------------------------------------------------------------------------------------------

class CheckState(Enum):
    OK = 0
    WARNING = 1
    CRITICAL = 2
    UNKNOWN = 3

# -------------------------------------------------------------------------------------------------

class Ior:

    ior_url  = None

    def __init__(self):

        self.args = {}
        self.parse_args()
        self.ior_url = self.args.ior_url


    def parse_args(self):
        """
          parse commandline parameters
        """
        p = argparse.ArgumentParser(description = 'Icinga2 plugin to check CoreMedia Cap Connections')

        p.add_argument('--url',
                    required = True,
                    dest = 'ior_url',
                    help = 'IOR url (http://localhost:40180/content-management-server/ior)' )

        self.args = p.parse_args()


    def __request_data(self, url):
      """
        executes get request to retrieve data from given url
      """

      logging.captureWarnings(True)

      try:
          # https://realpython.com/python-requests/#the-get-request
          response = get(url)
          # check response content type to determine which actuator api to be used
          if response.ok:
              return CheckState.OK, response, None
          else:
              response_json = response.json()
              response_message = response_json.get('message')
              return CheckState.CRITICAL, response, "%s - %s - %s" % ( response.status_code, url, response_message)

      except ConnectionRefusedError as e:
          logging.error('Failed to establish a new connection: {} '.format(url) )
          return CheckState.CRITICAL, None, msg

      except ConnectionError as e:
          msg = 'error fetching data from {}' . format(url)
          #logging.error('error fetching data from {}'.format(url))
          #logging.debug('error fetching data from {}'.format(url))
          return CheckState.CRITICAL, None, msg

      finally:
          logging.captureWarnings(False)

# -------------------------------------------------------------------------------------------------

    def check_result(self, rc, message, metrics = None):
        """

        """
        prefix   = rc.name
        message  = '{} - {}'.format(prefix, message)

        print(message)
        sys.exit(rc.value)


    def get_ior(self):
      """

      """
      state, response, error_message = self.__request_data(self.ior_url)

      message = ''

      if( state == CheckState.OK ):
          content_type = response.headers['Content-Type']
          content_body = response.text

          if( "text/plain" in content_type ):
              line = re.findall(r'IOR:', content_body)[0]

              if( line == "IOR:" ):
                  message = "IOR is available"
                  state = CheckState.OK
              else:
                  message = "request was successful, but the result contains no valid IOR"
                  state = CheckState.UNKNOWN

          else:
              message = "wrong content type: %s" % (content_type)
              state = CheckState.CRITICAL

      else:
          message = "%s" % (error_message)
          state = CheckState.CRITICAL

      self.check_result( state, message, {} )


    def check(self):
        self.get_ior()

# -------------------------------------------------------------------------------------------------

c = Ior()
c.check()

# -------------------------------------------------------------------------------------------------
