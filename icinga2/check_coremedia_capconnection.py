#!/usr/bin/env python3

from __future__ import print_function

import sys
import re
import argparse
import json
import coremedia_base

from enum import Enum

# -------------------------------------------------------------------------------------------------

class CheckState(Enum):
    OK = 0
    WARNING = 1
    CRITICAL = 2
    UNKNOWN = 3

# -------------------------------------------------------------------------------------------------

class CapConnection(coremedia_base.CoreMediaBase):
    """

    """
    host        = None
    jmx_port    = 0
    application = None

    def __init__(self):

        coremedia_base.CoreMediaBase.__init__(self)

        self.args = {}
        self.parse_args()

        self.host          = self.args.host
        self.jmx_port      = self.args.jmxport
        self.application   = self.args.application
        self.jolokia_url   = "http://%s:8080/jolokia/" % (self.host)
        self.spring_boot   = self.args.spring_boot

        if(self.spring_boot == 'autodetect'):
            self.spring_boot     = self._detect_spring_boot()


    def parse_args(self):
        """
          parse commandline parameters
        """
        p = argparse.ArgumentParser(description = 'Icinga2 plugin to check CoreMedia Cap Connections')

        p.add_argument('--host'       , required = True, help = 'hostname' )
        p.add_argument('--jmxport'    , required = True, type = int, help = 'JMX port (hint: CMS = 40199, MLS = 40299, RLS = 42099, ...)' )
        p.add_argument('--application', required = True, help = 'named application' )
        p.add_argument('--spring_boot', required = False, help = 'autodetection of sprint boot', default = True )

        self.args = p.parse_args()


    def __validate_application(self):
        """
          validate CoreMedia Application
        """
        valid_applications = [
            "workflow-server",
            "content-feeder",
            "user-changes",
            "elastic-worker",
            "caefeeder-preview",
            "caefeeder-live",
            "studio",
            "sitemanager",
            "headless-server",
            "eds",
            "cae-preview",
            "cae-live-1",
            "cae-live-2",
            "cae-live-3",
            "cae-live-4",
            "cae-live-5",
            "cae-live-6",
            "cae-live-7",
            "cae-live-8",
            "cae-live-9",
            "cae-live"]

        return self._validate_application( valid_applications, self.application )


    def __get_data(self):
        """
        read data from the jolokia server
        """
        valid, application = self.__validate_application()

        if(valid is False):
            print(application)
            sys.exit(CheckState.CRITICAL)

        if(application == 'eds'):
            self.spring_boot = True

        # CapListRepositoryAvailable
        #   whether the CapList repository is currently available
        # CapListRepositoryHealthy
        #   whether the CapList repository is healthy, i. e. either available or not required; always true for closed connections
        # CapListRepositoryRequired
        #   whether the CapList repository is required for this connection
        # ContentRepositoryAvailable
        #   whether the content repository is currently available
        # ContentRepositoryHealthy
        #   whether the content repository is healthy, i. e. either available or not required; always true for closed connections
        # ContentRepositoryRequired
        #   whether the content repository is required for this connection
        # LatestContentEventSequenceNumber
        #   the latest event sequence number for which invalidations have been processed
        # LatestReceivedContentEventSequenceNumber
        #   the latest event sequence number that has been received
        # Open
        #   whether the CapConnection has been opened and has not been closed since
        # Stable
        #   whether the CapConnection is stable, not having encountered an error that might induce an inconsistent state
        # WorkflowRepositoryAvailable
        #   whether the workflow repository is currently available
        # WorkflowRepositoryHealthy
        #   whether the workflow repository is healthy, i. e. either available or not required; always true for closed connections
        # WorkflowRepositoryRequired
        #   whether the workflow repository is required for this connection
        # Url
        #   the url that is to be/has been used to connect to the Content Server
        #
        data = {
            "Server": {
              "mbean": 'com.coremedia:type=CapConnection,application=%s' % (application),
              "attribute": [
                'CapListRepositoryAvailable',
                'CapListRepositoryHealthy',
                'CapListRepositoryRequired',
                'ContentRepositoryAvailable',
                'ContentRepositoryHealthy',
                'ContentRepositoryHealthy',
                'ContentRepositoryRequired',
                'WorkflowRepositoryAvailable',
                'WorkflowRepositoryHealthy',
                'WorkflowRepositoryRequired',
                'LatestContentEventSequenceNumber',
                'LatestReceivedContentEventSequenceNumber',
                'Open',
                'Stable',
                'Url']
            }
        }

        if( self.spring_boot ):
            if( application == 'cae-live' or application == 'cae-preview' or application == 'eds' ):
                data["Server"]["mbean"] = "com.coremedia:type=CapConnection,application=blueprint"
            else:
                data["Server"]["mbean"] = "com.coremedia:type=CapConnection,application=coremedia"

        return self.read_jmx_data(data)


    def get_capconnection(self):
        """
          get cap connection status
        """
        cap_connection = False
        state          = CheckState.CRITICAL
        message        = "no Cap Connection found."

        avail  = self.jolokia_availability(self.jolokia_url)
        status = avail.get('status')

        if( int(status) == 500 ):
            self.check_result(CheckState.CRITICAL, avail.get('message'), {})
            return

        data  = self.__get_data()
        # make pretty json
        # print(json.dumps( data, indent = 2 ))

        cap_connection_data = self.parse_bean_data(data, "CapConnection")
        status              = cap_connection_data.get('status')

        if(status == 200):
            cap_connection = cap_connection_data.get("Open")

            message = "Cap Connection <b>%s</b>" % ( "open" if cap_connection is True else "not exists" )

            state = CheckState.CRITICAL

            if(cap_connection is True):
                state = CheckState.OK

        self.check_result( state, message, {} )


    def __get_caplist_repository(self):
        """
          get caplist repository status
        """
        __state = 400
        __message = ''

        return __state, __message


    def __get_content_repository(self):
        """
          get content repository status
        """
        __state = 400
        __message = ''

        return __state, __message


    def __get_workflow_repository(self):
        """
          get workflow repository status
        """
        __state = 400
        __message = ''

        return __state, __message


    def check(self):
        self.get_capconnection()

# -------------------------------------------------------------------------------------------------

c = CapConnection()
c.check()

# -------------------------------------------------------------------------------------------------
