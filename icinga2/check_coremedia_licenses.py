#!/usr/bin/env python3

from __future__ import print_function

import sys
import argparse
import json
import coremedia_base

from datetime import datetime
from enum import Enum

# -------------------------------------------------------------------------------------------------

class CheckState(Enum):
    OK = 0
    WARNING = 1
    CRITICAL = 2
    UNKNOWN = 3

class LicenseValid(Enum):
    VALID_FROM = 1
    UNTIL_HARD = 2
    UNTIL_SOFT = 3

# -------------------------------------------------------------------------------------------------

class Licenses(coremedia_base.CoreMediaBase):

    host = ''
    jmx_port = 0

    CMS_PORT = 40199
    MLS_PORT = 40299
    RLS_PORT = 42099

    def __init__(self):

        coremedia_base.CoreMediaBase.__init__(self)

        self.args = {}
        self.parse_args()

        self.host        = self.args.host
        self.jmx_port    = self.args.jmxport
        self.jolokia_url = "http://%s:8080/jolokia/" % (self.host)

        self.warning       = self.args.warning
        self.critical      = self.args.critical

        self.spring_boot   = self.args.spring_boot

        if(self.spring_boot == 'autodetect'):
            self.spring_boot     = self._detect_spring_boot()


    def parse_args(self):
        """
          parse commandline parameters
        """
        p = argparse.ArgumentParser(description = 'Icinga2 plugin to check CoreMedia ContentServer Licenses')

        p.add_argument('--host',
                            required = True,
                            help = 'hostname for the ContentServer' )

        p.add_argument('--jmxport',
                            required = True,
                            type = int,
                            help = 'JMX port (hint: CMS = 40199, MLS = 40299, RLS = 42099)' )

        p.add_argument('--warning',
                            type = int,
                            default = 50,
                            help = 'how many days is the license still valid before a warning is triggered (default: 50)' )

        p.add_argument('--critical',
                            type = int,
                            default = 15,
                            help = 'how many days is the license still valid before it becomes critical (default: 15)' )

        p.add_argument('--spring_boot', required = False, help = 'autodetection of sprint boot', default = True )

        self.args = p.parse_args()



    def __validate_application(self):
        """

        """

        if( self.jmx_port == self.CMS_PORT ):
            application = 'content-management-server'
        elif( self.jmx_port == self.MLS_PORT ):
            application = 'master-live-server'
        elif( self.jmx_port == self.RLS_PORT ):
            application = 'replication-live-server'
        else:
          application = ''

        valid_applications = [
            "content-management-server",
            "master-live-server",
            "replication-live-server"]


        return self._validate_application( valid_applications, application )


    def __parse_license_date(self, data, type = LicenseValid.UNTIL_HARD ):
      """

      """
      if( type == LicenseValid.UNTIL_HARD ):
          raw_date = data['LicenseValidUntilHard']
      elif( type == LicenseValid.UNTIL_SOFT ):
          raw_date = data['LicenseValidUntilSoft']
      else:
          raw_date = data['LicenseValidFrom']

      current_datetime = datetime.now()
      license_datetime = datetime.fromtimestamp( raw_date / 1000 )

      difference       = license_datetime - current_datetime
      duration_in_s    = difference.total_seconds()
      days             = divmod(duration_in_s, 86400)

      date = license_datetime.strftime( "%d.%m.%Y" )

      return days[0], date


# -------------------------------------------------------------------------------------------------

    def license(self, type = LicenseValid.UNTIL_HARD):
        """

        """
        valid, application = self.__validate_application()

        if(valid is False):
            self.check_result( CheckState.CRITICAL, application, {} )
            return

        data = {
            # classic style
            "Server": {
                "mbean":  'com.coremedia:type=Server,application=%s' % ( application ),
                "attribute": [
                    'LicenseValidFrom',
                    'LicenseValidUntilHard',
                    'LicenseValidUntilSoft']
            }
        }


        if( self.spring_boot ):
            data["Server"]["mbean"] = "com.coremedia:type=Server,application=coremedia"

        result      = self.read_jmx_data(data)
        server_data = self.parse_bean_data(result, "Server")
        status      = server_data.get('status')

        if( status == 200 ):

          hard_days, hard_license_date = self.__parse_license_date(server_data, LicenseValid.UNTIL_HARD)
          soft_days, soft_license_date = self.__parse_license_date(server_data, LicenseValid.UNTIL_SOFT)

          # TODO #2
          # check license file

          # The CoreMedia license was valid until - expired xx days ago
          message = "CoreMedia license <b>was valid until %s</b> - <b>expired %d days ago</b>"

          if( soft_days > 0 ):
              message = "CoreMedia license is valid until %s - <b>%d days left</b>"

          message += " (grace time ends %s)"
          message  = message % ( soft_license_date, abs(soft_days), hard_license_date )

          # message += " | valid_soft=%d;%d;%d valid_hard=%d;%d;%d" % ( soft_days, args.warning, args.critical, hard_days, args.warning, args.critical )

          if( soft_days > self.warning or soft_days == self.warning ):
            state = CheckState.OK
          elif( soft_days > self.critical ):
            state = CheckState.WARNING
          else:
            state = CheckState.CRITICAL

        else:
          state = CheckState.UNKNOWN
          message = "unknown state. Maybe wrong or not running content-server?"

        self.check_result( state, message, {} )


    def check(self):
        self.license()


# -------------------------------------------------------------------------------------------------

c = Licenses()
c.check()

# -------------------------------------------------------------------------------------------------
