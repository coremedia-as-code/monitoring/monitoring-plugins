#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: ts=4:sw=4:et

# ------------------------------------------------------------------------------
# check_jolokia.py - A check plugin for Jolokia.
# Copyright (C) 2017-2020  Bodo Schulz <bodo@boone-schulz.de>
#
# Version: 1.0.0
#
# ------------------------------------------------------------------------------

import sys
import re
import json
import requests
import argparse
from enum import Enum


class CheckState(Enum):
    OK = 0
    WARNING = 1
    CRITICAL = 2
    UNKNOWN = 3

# adapted from https://github.com/nlm/nagplug/blob/master/nagplug/__init__.py
class Threshold(object):
    """

    """
    def __init__(self, threshold):
        self._threshold = threshold
        self._min = 0
        self._max = 0
        self._inclusive = False
        self._parse(threshold)

    def _parse(self, threshold):
        match = re.search(r'^(@?)((~|\d*):)?(\d*)$', threshold)

        if not match:
            raise ValueError('Error parsing Threshold: {0}'.format(threshold))

        if match.group(1) == '@':
            self._inclusive = True

        if match.group(3) == '~':
            self._min = float('-inf')
        elif match.group(3):
            self._min = float(match.group(3))
        else:
            self._min = float(0)

        if match.group(4):
            self._max = float(match.group(4))
        else:
            self._max = float('inf')

        if self._max < self._min:
            raise ValueError('max must be superior to min')

    def check(self, value):
        if self._inclusive:
            return True if self._min <= value <= self._max else False
        else:
            return True if value > self._max or value < self._min else False

    def __repr__(self):
        return '{0}({1})'.format(self.__class__.__name__, self._threshold)

    def __str__(self):
        return self._threshold


class CheckJolokia:
    """

    """
    VERSION = '1.0.0'

    def __init__(self):
        self.args = {}
        self.parse_args()
        self._baseurl   = f"http://{self.args.hostname}:{self.args.port}"

        self._state     = CheckState.OK

        # self._warning   = Threshold(self.args.threshold_warning)
        # self._criticial = Threshold(self.args.threshold_critical)


    def parse_args(self):
        p = argparse.ArgumentParser(description='Check command for Jolokia')

        p.add_argument("-H", "--hostname", required=False, help="Jolokia hostname", default='localhost')
        p.add_argument("-p", "--port"    , required=False, help="Jolokia HTTP port", default=8080)

        self.args = p.parse_args()


    def _fetch(self, uri):

        r = {}
        url= f"{self._baseurl}{uri}"
        try:
            response = requests.get(url,)

        except requests.exceptions.ConnectTimeout:
            self.check_result(
                CheckState.CRITICAL,
                "Could not connect to Jolokia: Connection timeout"
            )
        except requests.exceptions.SSLError:
            self.check_result(
                CheckState.CRITICAL,
                "Could not connect to Jolokia: Certificate validation failed"
            )
        except requests.exceptions.ConnectionError:
            self.check_result(
                CheckState.CRITICAL,
                "Could not connect to Jolokia: Failed to resolve hostname or service is not listenings"
            )
        except requests.exceptions.ConnectionRefusedError as e:
            self.check_result(
                CheckState.CRITICAL,
                'Failed to establish a new connection: {} '.format(url)
            )
        except requests.exceptions.ConnectionError as e:
            self.check_result(
                CheckState.CRITICAL,
                'error fetching data from {}' . format(url)
            )

        if response.ok:
            r = {
              "url": url,
              "code": response.status_code,
              "content_body": json.loads(response.text)
            }

        return response, r


    def check_result(self, rc, message, metrics=None):
        """

        """
        prefix   = rc.name
        message  = '{} - {}'.format(prefix, message)

        print(message)
        sys.exit(rc.value)


    def check_health(self):

        r, data = self._fetch('/jolokia')
        healthy = r.status_code == 200

        #print(json.dumps( data, indent = 2 ))
        version = data.get('content_body', {}).get('value', {}).get('agent',0)
        #print(version)
        msg     = f"Jolokia (version {version}) is healthy"

        if not healthy:
            msg = f"Jolokia health check failed ({r.status_code})"
            self._state = CheckState.CRITICAL

        self.check_result(self._state, msg, {})


    def check(self):
        self.check_health()


nc = CheckJolokia()
nc.check()










