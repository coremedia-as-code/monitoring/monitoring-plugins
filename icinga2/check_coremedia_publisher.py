#!/usr/bin/env python3

from __future__ import print_function

import sys
import re
import argparse
import json
import coremedia_base

from datetime import datetime
from enum import Enum

# -------------------------------------------------------------------------------------------------

class CheckState(Enum):
    OK = 0
    WARNING = 1
    CRITICAL = 2
    UNKNOWN = 3

class Runlevel(Enum):
    RUNLEVEL_OFFLINE = 0
    RUNLEVEL_MAINTENANCE = 1
    RUNLEVEL_ADMINISTRATION = 2
    RUNLEVEL_ONLINE = 3

# -------------------------------------------------------------------------------------------------

class Publisher(coremedia_base.CoreMediaBase):

    host = ''

    CMS_PORT = 40199

    def __init__(self):

        coremedia_base.CoreMediaBase.__init__(self)

        self.args = {}
        self.parse_args()

        self.host        = self.args.host
        self.jmx_port    = self.CMS_PORT
        self.application = "content-management-server"
        self.jolokia_url = "http://%s:8080/jolokia/" % (self.host)
        self.spring_boot   = self.args.spring_boot

        self.warning       = self.args.warning
        self.critical      = self.args.critical

        if(self.spring_boot == 'autodetect'):
            self.spring_boot     = self._detect_spring_boot()


    def parse_args(self):
        """
          parse commandline parameters
        """
        p = argparse.ArgumentParser(description = 'Icinga2 plugin to check CoreMedia latest publish to master-live-server')


        p.add_argument('--host',
                            required = True,
                            help = 'hostname for the RLS Server' )

        p.add_argument('--warning',
                            type = int,
                            default = 100,
                            help = 'warning how far the feeder may be behind the content server (default: 1000)' )

        p.add_argument('--critical',
                            type = int,
                            default = 250,
                            help = 'critical how far the feeder may be behind the content server (default: 2500)' )

        p.add_argument('--spring_boot', required = False, help = 'autodetection of sprint boot', default = True )

        self.args = p.parse_args()




    def __validate_application(self):
        """

        """
        valid_applications = [
            "content-management-server"]

        return self._validate_application( valid_applications, self.application )



    def __get_data(self):
        """

        """
        valid, application = self.__validate_application()

        if(valid is False):
            print(application)
            sys.exit(CheckState.CRITICAL)

        data = {
            "Publisher": {
                "mbean": 'com.coremedia:type=Publisher,target=target,application=content-management-server',
                # IorUrl
                #   the IOR URL of the Master Live Server to which content is published
                # Connected
                #   whether the Master Live Server of this publication target is currently connected
                # LastPublDate
                #   the time when the last publication was started, in milliseconds since 1970-01-01T00:00:00+00:00
                # LastPublResult
                #   either 'success' or 'failure' depending on whether the last completed publication was successful
                #
                "attribute": [
                  'IorUrl',
                  'Connected',
                  'LastPublDate',
                  'LastPublResult']
              }
        }

        if( self.spring_boot ):
            data["Publisher"]["mbean"] = "com.coremedia:type=Publisher,target=target,application=coremedia"

        return self.read_jmx_data(data)

# -------------------------------------------------------------------------------------------------

    def publisher_state(self):
        """
          get publisher state
        """

        state   = CheckState.UNKNOWN
        message = "no data found."

        data  = self.__get_data()
        # make pretty json
        # print(json.dumps( data, indent = 2 ))

        publisher_data = self.parse_bean_data(data, "Publisher")

        status         = publisher_data.get('status', 404)

        if(status == 200):

            connected                = publisher_data.get("Connected")
            last_publication_result  = publisher_data.get("LastPublResult")
            last_publication_date    = publisher_data.get("LastPublDate")
            ior_url                  = publisher_data.get("IorUrl")
            ior_data                 = self.parse_url(ior_url)

            mls_hostname = ior_data['host']

            if(connected is True):
                state  = CheckState.OK;

                message = "connected to MLS: <b>%s</b> - last publication result: <b>%s</b>" % ( mls_hostname, last_publication_result )

                if( last_publication_date != None ):

                    current_datetime = datetime.now()
                    publish_datetime = datetime.fromtimestamp( last_publication_date / 1000 )

                    difference       = publish_datetime - current_datetime
                    duration_in_s    = difference.total_seconds()
                    days             = divmod(duration_in_s, 86400)

                    date             = publish_datetime.strftime( "%d.%m.%Y %H:%M:%S" )

                    message = "connected to MLS: <b>%s</b> - last publication %s with result: <b>%s</b>" % ( mls_hostname, date, last_publication_result )

            else:
                state = CheckState.CRITICAL
                message = "CRITICAL - not connected to MLS: <b>%s</b>" % ( mls_hostname )

        self.check_result( state, message, {} )


    def check(self):
        self.publisher_state()


# -------------------------------------------------------------------------------------------------

c = Publisher()
c.check()

# -------------------------------------------------------------------------------------------------
